Kadima is the Hebrew for the word "forward" and that's exactly what we want to do... Move forward in the hair and skin industry. 

For years, women and men alike have been advertised all of these commercial brand products that contain ingredients that can be harmful to their skin and hair. 

KadimaOBP products contains no man-made ingredients and it isn't mass produced in a warehouse. Each product is handmade with great care to ensure you get all the benefits of using natural and organic products. 

Visit us today to begin your journey of beautiful hair and skin you desire and deserve!

Website : https://KadimaOBP.com
